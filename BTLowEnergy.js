import React, { useState, useEffect } from "react";
import { Text, View, Button } from "react-native";
import { BleManager } from "react-native-ble-plx";

const BTLowEnergy = () => {
  const [bleManager, setManager] = useState();
  const [scanning, setScanning] = useState(false);
  const [results, setResults] = useState();
  useEffect(() => {
    setManager(new BleManager());
    return () => {
      if (bleManager) {
        bleManager.destroy();
      }
    };
  }, []);

  useEffect(() => {
    if (scanning) {
      bleManager.startDeviceScan(null, {}, (err, device) => {
        if (err) {
          alert(err.message);
          return;
        }
        setResults(device);
      });
    } else {
      if(bleManager){
        bleManager.stopDeviceScan();
      }
    }
  }, [scanning]);

  return (
    <View>
      <Text>BTLowEnergy</Text>
      <Button
        title={`${scanning ? "Stop Scanning" : "Start Scanning"}`}
        onPress={() => setScanning(!scanning)}
      />
      {results && <Text>{results.name}</Text>}      
    </View>
  );
};

export default BTLowEnergy;
